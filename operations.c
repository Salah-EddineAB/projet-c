#include "listes.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

struct List *Init()
{
  struct List *list =  malloc(sizeof(*list));
  struct Contact *contact = malloc(sizeof(*contact));

  contact->num = 0;
  contact->next = NULL;
  
  list->head = contact;

  return list;
}

void add(struct List *list, uint32_t number)
{
  struct Contact *contact = malloc(sizeof(*contact));

  if (contact == NULL) {
    printf("Problème lors de la création du contact");
  } else {
    contact->num = number;
    contact->next = list->head;

    list->head = contact;
  }
}
