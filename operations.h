#ifndef _LISTES_H_
#define _LISTES_H_

#include <stdint.h>

struct Contact {
  uint32_t num;
  struct Contact *next;
};

struct List {
  struct Contact *head;
};

extern struct List *Init();

extern void add(struct List *list, uint32_t number);

#endif
